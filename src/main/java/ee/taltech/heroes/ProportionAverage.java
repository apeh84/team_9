package ee.taltech.heroes;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProportionAverage {
	public static void main(String[] args) {
		SpringApplication.run(ProportionAverage.class, args);
	}
}

