package java.ee.taltech.heroes.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.ee.taltech.heroes.resource.FailResponse;
import java.ee.taltech.heroes.resource.SuccessResponse;
import java.ee.taltech.heroes.service.AverageService;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class CalculationController {
private static final String DELIMITER = ",";
    private AverageService averageService;

    public CalculationController(AverageService averageService) {
        this.averageService = averageService;
    }

    @GetMapping("/calculate")
    @ResponseBody
    public SuccessResponse<Double> getAverage(@RequestParam(value = "values") int[] data) {
        String loggedValues = Arrays.stream(data)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(DELIMITER));
        log.info("GET request on \"/calculate\" value=[" + loggedValues + "] was initiated.");

        if (data.length == 0) {
            throw new RuntimeException("Empty array");
        }

        double average = averageService.proportion(data);
        log.info("GET request on \"/calculate\" value=[" + loggedValues + "] resulted with " + average + ".");
        return new SuccessResponse<>(average);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public FailResponse<Object> handleException(Throwable throwable) {
        String finalMessage = throwable instanceof MethodArgumentTypeMismatchException
                ? "Invalid argument type. Expected 'entries' of type int[]."
                : throwable.getMessage();

        log.error(finalMessage);
        return new FailResponse<>(new Object() {
            public String message = finalMessage;
        });
    }
}