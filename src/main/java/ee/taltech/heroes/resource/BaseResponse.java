package java.ee.taltech.heroes.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data

@AllArgsConstructor
abstract class BaseResponse<T> {
    private T data;
}
