package java.ee.taltech.heroes.resource;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
@Data
@Getter
public class ErrorResponse<T> extends BaseResponse<T> {
    private String status = "error";

    @Setter
    @NonNull
    private String message;

    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer code;

    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public ErrorResponse(String message, Integer code, T data) {
        super(data);

        this.message = message;
        this.code = code;
    }
}

