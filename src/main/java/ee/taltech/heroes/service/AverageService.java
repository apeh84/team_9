package java.ee.taltech.heroes.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
@Service
public class AverageService {
private static final double divisor = 2 ;

    public double getMin(int[] data) {
        if (data.length == 0) {
            throw new RuntimeException("Array can not be empty");
        } else if(data.length < 2) {
            throw new RuntimeException("Array can not have < 2 entries");
        } else {
            Arrays.sort(data);
        }
        return data[0];
    }

    public double getMax(int[] data) {
        if (data.length == 0) {
            throw new RuntimeException("Array can not be empty");
        } else if(data.length < 2) {
            throw new RuntimeException("Array can not have < 2 entries");
        } else {
            Arrays.sort(data);
        }
        return data[data.length - 1];
    }

    public double generalAverage(int[] data) {
        if (data.length == 0) {
            throw new RuntimeException("Array can not be empty");
        }
        double total = 0;
        for (int i = 0; i < data.length; i++) {
            total = total + data[i];
        }
        return total / data.length;
    }

    public double minMaxAverage(int[] data) {
        if (data.length == 0) {
            throw new RuntimeException("Array can not be empty");
        }
        double min = getMin(data);
        double max = getMax(data);
        double total = min + max;
        return total / divisor;
    }

    public double proportion(int[] data) {
        if (data.length == 0) {
            throw new RuntimeException("Array can not be empty");
        }
        return  generalAverage(data) / minMaxAverage(data);
    }

}
